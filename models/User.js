const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const userSchema = new mongoose.Schema({
    email: {
        type: String,
        required: true,
        index: { unique: true }
        //TODO: dodac tu validator na mail
    },
    password: {
        type: String,
        required: true
    },
    tradingActive: {
        type: Boolean,
        required: true,
        default: false
    },
    apiKey: {
        type: String,
        required: false
    },
    apiSecret: {
        type: String,
        required: false
    },
    interval: {
        type: Number,
        required: false
    },
    dollarAmount: {
        type: Number,
        required: false
    },
    triggerPercent: {
        type: Number,
        required: false
    },
    initialBTCPrice: {
        type: Number,
        required: false
    },
    initialBTCAmount: {
        type: String,
        required: false
    },
    tokens: [{
        token: {
            type: String,
            required: true
        }
    }]
})

userSchema.pre('save', async function (next) {
    const user = this
    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 8)
    }
    next()
})

userSchema.methods.generateAuthToken = async function() {
    const user = this
    const token = jwt.sign({_id: user._id}, process.env.JWT_KEY)
    return token
}

userSchema.statics.findByCredentials = async (email, password) => {
    const user = await User.findOne({ email} )
    if (!user) {
        throw new Error({ error: 'Invalid login credentials' })
    }
    const isPasswordMatch = await bcrypt.compare(password, user.password)
    if (!isPasswordMatch) {
        throw new Error({ error: 'Invalid login credentials' })
    }
    return user
}

userSchema.statics.updateSettings = async (user, data) => {
    const updatedUser = User.findOneAndUpdate({_id: user._id}, {
        tradingActive: data.trading_active,
        apiKey: data.api_key,
        apiSecret: data.secret_key,
        interval: data.interval,
        dollarAmount: data.dollar_amount,
        triggerPercent: data.trigger_percent,
        initialBTCPrice: data.initial_btc_price,
        initialBTCAmount: data.initial_btc_amount,
    }, { upsert: true }, (err, res) => {
        if (err) return false
        return updatedUser
    })
}

const User = mongoose.model('User', userSchema)

module.exports = User
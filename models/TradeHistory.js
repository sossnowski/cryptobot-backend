const mongoose = require('mongoose');
const jwt = require('jsonwebtoken')


const tradingHistorySchema = new mongoose.Schema({
    userId: {
        type: String,
        required: true
    },
    symbol: {
        type: String,
        required: true
    },
    price: {
        type: Number,
        required: true
    },
    qty: {
        type: Number,
        required: true
    },
    quoteQty: {
        type: Number,
        required: true
    },
    commision: {
        type: Number,
        required: true
    },
    time: {
        type: Number,
        required: true
    },
    isBuyer: {
        type: Boolean,
        required: true
    },
    isMaker: {
        type: Boolean,
        required: true
    }
});

tradingHistorySchema.statics.getUserTradingHistory = async (user, currencyPair) => {
    const tradingHistory = await TradingHistory.find({userId: user._id, symbol: currencyPair })
    return tradingHistory;
}

const TradingHistory = mongoose.model('TradingHistory', tradingHistorySchema)

module.exports = TradingHistory;
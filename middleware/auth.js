const jwt = require('jsonwebtoken')
const User = require('../models/User')

const auth = async(request, response, next) => {
    try {
        const token = request.header('Authorization').replace('Bearer ', '')
        const data = jwt.verify(token, process.env.JWT_KEY)
        const user = await User.findOne({ _id: data._id})
        if (!user) {
            throw new Error()
        }
        request.user = user
        request.token = token
        next()
    } catch (error) {
        response.status(401).send({ error: 'Not authorized to access this resource' })
    }

}

module.exports = auth
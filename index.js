const express = require('express')
const userRouter = require('./routes/users')
const dotenv = require('dotenv')
dotenv.config();
require('./db/connect')

const app = express()

app.use(express.json())
app.use(userRouter)

app.listen(3050, () => {
    console.log(`Server running on port 3050`)
})
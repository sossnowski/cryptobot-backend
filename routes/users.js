const express = require('express')
const auth = require('../middleware/auth')
const User = require('../models/User')
const TradingHistory = require('../models/TradeHistory')

const router = express.Router()

router.post('/users', async (request, response) => {
    try {
        const user = new User(request.body)
        await user.save()
        const token = await user.generateAuthToken()
        response.status(201).send({ user, token })
    } catch (error) {
        response.status(400).send(error)
    }
})

router.post('/users/login', async (request, response) => {
    try {
        const { email, password } = request.body
        const user = await User.findByCredentials(email, password)
        if (!user) {
            return response.status(401).send({error: 'Login failed! Check authentication credentials'})
        }
        const token = await user.generateAuthToken()
        response.send({ user, token })
    } catch (error) {
        response.status(400).send(error)
    }
})

router.get('/users/me', auth, async (request, response) => {
    const user = request.user
    response.json({
        data: {
            email: user.email,
            trading_active: user.tradingActive,
            api_key: user.apiKey,
            secret_key: user.apiSecret,
            interval: user.interval,
            dollar_amount: user.dollarAmount,
            trigger_percent: user.triggerPercent,
            initial_btc_price: user.initialBTCPrice,
            initial_btc_amount: user.initialBTCAmount,
        }
    })
})

router.post('/users/logout', auth, async (request, response) => {
    try {
        request.user.tokens = request.user.tokens.filter((token) => {
            return token.token !== request.token
        })
        await request.user.save()
        response.send()
    } catch (error) {
        response.status(500).send(error)
    }
})

router.put('/settings', auth, async (request, response) => {
    try {
        const user = await User.updateSettings(request.user, request.body)
        return response.status(201).json(user)
    } catch (error) {
        return response.status(500).send(error)
    }
})

router.get('/trades/:currencyPair', auth, async (request, response) => {
    const { currencyPair } = request.params
    try {
        const tradingHistory = await TradingHistory.getUserTradingHistory(request.user, currencyPair)
        if (tradingHistory.length === 0) return response.json({ data: [] })
        return response.json({ data: tradingHistory })
    } catch (error) {
        return response.status(500).json(error)
    }
})

module.exports = router
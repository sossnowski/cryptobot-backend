const credentials = require('../.env');
const constants = require('../constants');
var mongoose = require('mongoose');
const tradeHistorySchema = require('../models/tradeHistory');
const binance = require('node-binance-api')().options({
    APIKEY: credentials.apikey,
    APISECRET: credentials.apisecret,
    useServerTime: true // If you get timestamp errors, synchronize to server time at startup
});


//from api binance
tradeHistory = () => {
    binance.trades(constants.tradeHistoryPair, (error, trades, symbol) => {
        console.log(symbol+" trade history", trades);
    });
};

//from db

tradeHistoryFromDB = () => {
    let trades = mongoose.model('tradingHistory', tradeHistorySchema);
    trades.find({}, function (err, result) {
        console.log(result)
        return result;
    })
};


module.exports.tradeHistoryFromDB = tradeHistoryFromDB;
module.exports.tradeHistory = tradeHistory;
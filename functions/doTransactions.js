// const constants = require('../constants');
const TradingHistory = require('../models/TradeHistory')

getCurrentPrice = (currencyPair, binance) => {
    return new Promise(resolve => {
        binance.prices((error, ticker) => {
            resolve(ticker[currencyPair]);
        });
    });
};

getCurrentBalance = (currency, binance) => {
    return new Promise(resolve => {
        binance.balance((error, balances) => {
            if (error) return console.error(error);
            resolve(balances[currency].available)
        });
    });
};

getEarnedMoney = (binance) => {
    return new Promise((resolve, reject) => {
        binance.balance((error, balances) => {
            if (error) return console.error(error);
            resolve(balances['USDT'].available);
        });
    });
};

createTradeObjectToDB = (trade, userId) => {
    let newTrade = {
        userId: userId,
        symbol: trade.symbol,
        price: trade.price,
        qty: trade.qty,
        quoteQty: trade.quoteQty,
        commision: trade.commission,
        time: trade.time,
        isBuyer: trade.isBuyer,
        isMaker: trade.isMaker,
    }
    return newTrade
}

doTransaction = async (price, amount, constants, binance) => {
    if (amount * price >= constants.amountOfMoneyToSell()) {
        let quantity = ((amount * price) - constants.startedAmount) / price;
        binance.marketSell(constants.currencyPair, quantity, (error, response) => {
            if (error) return console.error(error.body)
            console.log("Market Buy response", response);
            binance.trades(constants.currencyPair, async (error, trades, symbol) => {
                if (error) return console.error(error.body)
                try {
                    const trade = new TradingHistory(createTradeObjectToDB(trades[trades.length - 1], constants.userId))
                    await trade.save()
                    console.log('successfully saved')
                } catch (error) {
                    console.log(error)
                }
            });
        })
    } else if (amount * price <= constants.amountOfMoneyToBuy()) {
        getEarnedMoney(binance)
            .then(amountYouEarned => {
                if (amountYouEarned >= constants.startedAmount - (price * amount)) {
                    let quantity = (constants.startedAmount - (price * amount)) / price;
                    binance.marketBuy(constants.currencyPair, quantity, (error, response) => {
                        if (error) return console.error(error.body)
                        console.log("Market Buy response", response);
                        binance.trades(constants.currencyPair, async (error, trades, symbol) => {
                            if (error) return console.error(error.body)
                            try {
                                const trade = new TradingHistory(createTradeObjectToDB(trades[trades.length - 1], constants.userId))
                                await trade.save()
                                console.log('successfully saved')
                            } catch (error) {
                                console.log(error)
                            }
                        });
                    })
                } else {
                    let quantity = amountYouEarned;
                    binance.marketBuy(constants.currencyPair, quantity, (error, response) => {
                        if (error) return console.error(error.body)
                        console.log("Market Buy response", response);
                        binance.trades(constants.currencyPair, async (error, trades, symbol) => {
                            if (error) return console.error(error.body)
                            try {
                                const trade = new TradingHistory(createTradeObjectToDB(trades[trades.length - 1], constants.userId))
                                await trade.save()
                                console.log('successfully saved')
                            } catch (error) {
                                console.log(error)
                            }
                        });
                    })
                }

            });
    }
};

let price = 0;
let amount = 0;

work = async (constants) => {
    const binance = require('node-binance-api')().options({
        APIKEY: constants.apiKey,
        APISECRET: constants.apiSecret,
        useServerTime: true // If you get timestamp errors, synchronize to server time at startup
    });
    // console.log(constants)
    Promise.all([
        getCurrentPrice(constants.currencyPair, binance),
        getCurrentBalance(constants.currency, binance)
    ])
        .then(response => {
            price = response[0];
            amount = response[1];

            doTransaction(price, amount, constants, binance)
                .then(result => {return result})
        })
        .catch(error => {return error.body})


};


module.exports = work;
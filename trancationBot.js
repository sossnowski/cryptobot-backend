const dotenv = require('dotenv')
const doTransactions = require('./functions/doTransactions')
const User = require('./models/User')
const NodeCache = require( "node-cache" );
const myCache = new NodeCache();
var cron = require('node-cron');
dotenv.config();
require('./db/connect')

const cacheKey = process.env.CACHE_KEY;

var cacheObject = myCache.get("cacheKey");
if (cacheObject === undefined) {
    cacheObject = {timeFromStart: 0}
    let success = myCache.set( cacheKey, cacheObject, 0 )
    if (!success) throw new Error('error while adding value to cache')
}

transactionBot = async () => {
    try {
        User.find({}, (error, results) => {
            if (error) throw new Error("cant get users from db")
            for (let i = 0; i < results.length; i++) {
                if (results[i].tradingActive === false) continue
                work(results[i]).then(/*result => console.log(result)*/)
            }
            cacheObject.timeFromStart += 60
            if (cacheObject.timeFromStart === 86400) cacheObject.timeFromStart = 0
            let success = myCache.set( cacheKey, cacheObject, 0 );
            if (!success) throw new Error('error while adding value to cache')
        })
    } catch (error) {
        return console.log(error.body)
    }
}
const work = async (settingsFromDB) => {
    try {
        const constants = {
            triggeredPercent: settingsFromDB.triggerPercent,
            startedAmount: settingsFromDB.dollarAmount,
            timeInterval: settingsFromDB.interval,
            currency: 'BTC',
            currencyPair: 'BTCUSDT',
            apiKey: process.env.APIKEY,//settingsFromDB.apiKey, //in db are not true values
            apiSecret: process.env.APISECRET,//settingsFromDB.apiSecret,
            amountOfMoneyToSell: function () {
                return this.startedAmount + (this.startedAmount * this.triggeredPercent / 100)
            },
            amountOfMoneyToBuy: function () {
                return this.startedAmount - (this.startedAmount * this.triggeredPercent / 100)
            },
            tradeHistoryPair: 'BTCUSDT',
            userId: settingsFromDB._id,
        };

        cacheObject = myCache.get(cacheKey);
        if (cacheObject === undefined) {
            throw new Error('cant get cache time');
        }
        if (cacheObject.timeFromStart % constants.timeInterval === 0) {
            try {
                doTransactions(constants)
            } catch(error) {
                console.log(error.body)
            }

        }
    }catch (error) {
        console.log(error.body);
    }
}

// transactionBot();

// setInterval(transactionBot, 6000);

cron.schedule('* * * * *', () => {
    transactionBot()
});



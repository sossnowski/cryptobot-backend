const constants = {
    triggeredPercent: 6,
    startedAmount: 1000,
    timeInterval: 1,
    currency: 'BTC',
    currencyPair: 'BTCUSDT',
    amountOfMoneyToSell: function() {
        return this.startedAmount + (this.startedAmount * this.triggeredPercent / 100)
    },
    amountOfMoneyToBuy: function() {
        return this.startedAmount - (this.startedAmount * this.triggeredPercent / 100)
    },
    tradeHistoryPair: 'BTCUSDT',
};

module.exports = constants;